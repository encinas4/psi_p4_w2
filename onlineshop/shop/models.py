    # -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.template.defaultfilters import slugify


# Create your models here.
# Se crean las clases categoria y productos con todos los campos que se le van a anyadir en la base de datos
class Category(models.Model):
    catName = models.CharField(max_length=128, unique=True, null=False)  # null = TRUE para que no sea NOT NULL
    catSlug = models.SlugField(unique=True, null=False)

    class Meta:
        verbose_name_plural = 'Categories'

    def save(self, *args, **kwargs):
        self.catSlug = slugify(self.catName)
        super(Category, self).save(*args, **kwargs)

    def __str__(self):
        return self.catName

    def __unicode__(self):
        return self.catName


class Product(models.Model):
    #category = models.ForeignKey(Category,default = 1, on_delete=models.SET_DEFAULT)
    category = models.ForeignKey(Category, null=False)
    prodName = models.CharField(max_length=128, unique=True, null=False)
    prodSlug = models.SlugField(unique = True, null=False)
    image = models.ImageField(upload_to='../media', null=False)
    description = models.CharField(max_length=128, null=False)
    price = models.DecimalField(max_digits=10, decimal_places=2, null=False)
    stock = models.IntegerField(default=1, null=False)
    availability = models.BooleanField(default=True, null=False)
    created = models.DateTimeField(auto_now_add=True)	#auto_now_add=True
    updated = models.DateTimeField(auto_now=True)	#auto_now=True

    def save(self, *args, **kwargs):
        self.prodSlug = slugify(self.prodName)
        super(Product, self).save(*args, **kwargs)


    def __str__(self):  # For Python 2, use __unicode__ too
        return self.prodName

    def __unicode__(self):
        return self.prodName
