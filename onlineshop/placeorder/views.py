# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect
from django.http import HttpResponse
from forms import OrderCreateForm
from django import forms
from shoppingcart.shoppingcart import ShoppingCart
from shop.models import Product,Category
from models import OrderLine, Order

# Crea el order y le pasa toda la informacion a la template createOrder.html
def createOrder(request):
	shoppingcart = ShoppingCart(request)
	order_form = OrderCreateForm()
	product_ids = shoppingcart.cart.keys()
	products = Product.objects.filter(id__in=product_ids)

	return render(request, 'placeorder/createOrder.html',{'order_form':order_form, 'shopping_cart':shoppingcart, 'products': products})

# Crea y almacena una instancia de Order y tantas instancias de OrderLine como necesite
def confirmOrder(request):

	if request.method == 'POST':

		#capturamos los datos del form	
		formu = OrderCreateForm(request.POST)
		if formu.is_valid():
			firstName = formu.cleaned_data['firstName']
			familyName = formu.cleaned_data['familyName']
			email = formu.cleaned_data['email']
			address = formu.cleaned_data['address']
			zip = formu.cleaned_data['zip']
			city = formu.cleaned_data['city']
			order=formu.save()

		shoppingcart = ShoppingCart(request)
		#for prod in shoppingcart:
		#	OrderLine.objects.create(order=order, product=prod['product'], units=prod['units'], pricePerUnit=prod['price'])

		product_ids = shoppingcart.cart.keys()
		products = Product.objects.filter(id__in=product_ids)
		for p in products:
			p_id = str(p.id)
			producto = shoppingcart.cart[p_id]
			valor = p.stock - producto['units']
			if(valor > 0):
				p.stock =valor
				OrderLine.objects.create(order=order, product=p, units=producto['units'], pricePerUnit=producto['price'])
				p.save()
				shoppingcart.clear()
				return render(request, 'placeorder/confirmOrder.html',{'order':order})
			else:
				Order.objects.filter(id=order.id).delete()
				shoppingcart.clear()
				return render(request, 'shoppingcart/errorstock.html')


	
